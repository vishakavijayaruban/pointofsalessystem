Imports System
Imports System.ComponentModel

Module Program
    Sub Main(args As String())
        dim totalCost As Decimal
        Console.WriteLine("Enter your cost. X to stop.")
        Dim costring = Console.ReadLine()
        While costring <> "x" AND costring <> "X"
            Dim cost = ConvertStringToReal(costring)
            totalCost = totalCost + cost
            Console.WriteLine("Enter your cost. X to stop.")
            costring = Console.ReadLine()
        End While
        Console.WriteLine(totalCost)
    End Sub
    
    Function ConvertStringToReal(input as String) As Decimal
      dim cost = System.Convert.ToDecimal(input)
        Return cost
    End Function
    
End Module
